import React from "react"
import {componentAsPrioritisedFunction, widgets} from "../component/index"
import {Col, FormGroup, Input, Label} from "reactstrap"
import injectSheet from "react-jss"

const styles = {
    filter: {
        fontSize: "smaller",
        textTransform: "uppercase",
        color: "lightgray"
    }
}

const FilterPanel = injectSheet(styles)(function FilterPanel(props) {
    const {classes, bag, doUpdate} = props
    return <Col className={classes.column}>
        <FormGroup>
            <Label>Filter</Label>
            <Input type="text" value={bag._searchTerm || ""} onChange={(e) => {
                bag._searchTerm = e.target.value
                doUpdate()
            }} placeholder="Filter Questions..."/>
        </FormGroup>
    </Col>
})

const FilterLabel = injectSheet(styles)(function FilterPanel(props) {
    const {classes, bag} = props
    return <Col className={classes.filter}>
        <Label>Filtering on: "{bag._searchTerm}"</Label>
    </Col>
})

widgets.on("filter.questions", function (context) {
    const {bag: {_searchTerm = ""}, answers}= context
    context.questions = context.questions.filter(question => {
        let searchString = _searchTerm.toLowerCase()
        return (question.question || "").toLowerCase().indexOf(searchString) !== -1
            || (answers[question.name] || "").toLowerCase().indexOf(searchString) !== -1
    })
})

widgets.on("edit.*", function ({size, design, layout, bag: {_searchTerm = ""}}) {
    if (design.questions && size !== "xs") {
        layout.right.push(componentAsPrioritisedFunction(FilterPanel))
        _searchTerm && layout.footer.push(componentAsPrioritisedFunction(FilterLabel))
    }
})
