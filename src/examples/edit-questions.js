import {widgets, inPriorityOrder} from "../component/index"
import {ButtonDropdown, Col, DropdownItem, DropdownMenu, DropdownToggle, FormGroup, Input, Label, Row} from "reactstrap"
import {FiPlus} from "react-icons/fi"
import React, {useState} from "react"
import {generate} from "shortid"
import injectSheet from "react-jss"
import classnames from "classnames"
import {heading} from "./styles"
import {componentAsPrioritisedFunction} from "../component/index"

function createDefaultQuestion(props) {
    return Object.assign({
        id: generate(),
        name: "",
        question: ""
    }, props)
}

export {createDefaultQuestion}

const styles = {
    questionRow: {
        margin: {
            top: 5
        }
    },
    heading
}

const AddQuestion = injectSheet(styles)(({classes, doUpdate, design, types}) => {
    const [open, setOpen] = useState(false)
    return (
        <Row key={"questiontypes"} className={classnames(classes.questionRow, "no-gutters")}>
            <Col>&nbsp;</Col>
            <Col xs="auto">
                <ButtonDropdown direction="left" size="sm" isOpen={open} toggle={() => setOpen(!open)}>
                    <DropdownToggle color="success">
                        <FiPlus/> Question
                    </DropdownToggle>
                    <DropdownMenu>
                        {types.sort(inPriorityOrder).map((type, index) => (
                            <DropdownItem onClick={() => {
                                design.push(type.create())
                                doUpdate()
                            }} key={index}>
                                {type.name}
                            </DropdownItem>
                        ))}
                    </DropdownMenu>
                </ButtonDropdown>
            </Col>
        </Row>
    )
})

widgets.on("inline.questions", function ({design, document, inline, ok}) {
    let types = []
    widgets.emit("questions.types", types)
    ok && inline.push(props => <AddQuestion types={types} {...props}/>)
})

const QuestionInfo = injectSheet(styles)(function QuestionInfo({focus: {selectedQuestion}, doUpdate, classes}) {
    return (
        <>
            <h4 className={classes.heading} key="title">Question</h4>
            <FormGroup row key="name">
                <Label sm={2}>Name</Label>
                <Col sm={10}>
                    <Input type="text" value={selectedQuestion.name} onFocus={e=>e.target.select()} onChange={event => {
                        selectedQuestion.name = event.target.value
                        doUpdate()
                    }}/>
                </Col>
            </FormGroup>
            <FormGroup row key="question">
                <Label sm={2}>Question</Label>
                <Col sm={10}>
                    <Input type="text" value={selectedQuestion.question} onFocus={e => e.target.select()} onChange={(event) => {
                        selectedQuestion.question = event.target.value
                        doUpdate()
                    }}/>
                </Col>
            </FormGroup>
        </>
    )
})

widgets.on("edit.questionnaire", function ({editor, focus: {selectedQuestion}}) {
    selectedQuestion && editor.tabs.general.content.push(componentAsPrioritisedFunction(QuestionInfo, 2))
})
