import React, {useState} from "react"
import {componentAsPrioritisedFunction, widgets} from "../component/index"
import {Col, FormGroup, Input, Label} from "reactstrap"
import injectSheet from "react-jss"

const styles = {
    column: {
        marginTop: 12,
        "& input": {
            fontSize: "smaller",
            padding: ".175em .75em"
        }
    },
    filter: {
        fontSize: "smaller",
        textTransform: "uppercase",
        color: "lightgray"
    }
}

const FilterPanel = injectSheet(styles)(function FilterPanel(props) {
    const {classes, bag, queueUpdate} = props
    const [searchTerm, setSearchTerm] = useState(bag.searchTerm || "")
    return <Col className={classes.column} xs={12}>
        <FormGroup>
            <Input type="text" value={searchTerm} onChange={(e) => {
                setSearchTerm(e.target.value)
                bag.searchTerm = e.target.value || ""
                queueUpdate()
            }} placeholder="Filter Dashboard..."/>
        </FormGroup>
    </Col>
})

widgets.on("filter.dashboardItems", function (context) {
    const {bag: {searchTerm = ""}} = context
    context.dashboardItems = context.dashboardItems.filter(item => {
        let searchString = searchTerm.toLowerCase()
        return item.title.toLowerCase().indexOf(searchString) !== -1
            || item.description.toLowerCase().indexOf(searchString) !== -1
    })
})

widgets.on("edit.*", function ({design, layout, bag: {searchTerm}}) {
    if (design.dashboardItems) {
        layout.headerCentre.push(componentAsPrioritisedFunction(FilterPanel, 10))
        searchTerm && layout.footer.push(componentAsPrioritisedFunction(FilterLabel))
    }
})

const FilterLabel = injectSheet(styles)(function FilterPanel(props) {
    const {classes, bag} = props
    return <Col className={classes.filter}>
        <Label>Filtering on: "{bag.searchTerm}"</Label>
    </Col>
})

