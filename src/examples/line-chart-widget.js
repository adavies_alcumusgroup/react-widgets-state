import React from "react"
import {componentAsPrioritisedFunction, widgets} from "../component/index"
import {createDefaultItem} from "./dashboard-items"
import {Card, CardBody, CardText, CardTitle} from "reactstrap"
import {CartesianGrid, Line, LineChart, ResponsiveContainer, XAxis, YAxis} from "recharts"
import injectSheet from "react-jss"
import color from "./colors"
import {DUMMY_DATA} from "./example/dummy"

widgets.on("dashboardItems.types", types => {
    types.push({
        name: "Line Chart",
        create: () => createDefaultItem({type: "line", data: [], color: "#fcfcfc"}),
    })
})

const style = {
    card: {
        marginBottom: 32,
        overflow: "hidden",
    },
    chart: {},
}

const Render = injectSheet(style)(({root, value, setValue, design: {item}, classes}) => {
    let data = item.data.length ? item.data : DUMMY_DATA
    const {height = 3} = item
    return (

        <Card className={classes.card} style={{height: root.fillHeight ? "calc(100% - 32px)" : undefined}}>
            <ResponsiveContainer height={height * 75}>
                <LineChart style={{background: item.color || "#fff"}}
                           className={classes.chart}
                           data={data}
                           margin={{top: 20, right: 10, left: 0, bottom: 0}}>
                    <Line nameKey={item.series || "name"} strokeWidth={2} stroke={color(0, undefined, item.adjustColor)}
                          type="monotone"
                          dataKey={item.value || "value"}/>
                    <CartesianGrid strokeDasharray="3 3"/>
                    <XAxis dataKey={item.series || "name"}/>
                    <YAxis/>
                </LineChart>
            </ResponsiveContainer>
            {(item.title || item.description) && <CardBody>
                {!!item.title && <CardTitle dangerouslySetInnerHTML={{__html: item.title}}/>}
                {!!item.description && <CardText dangerouslySetInnerHTML={{__html: item.description}}/>}
            </CardBody>}
        </Card>

    )
})


widgets.on("editor.dashboardItem.line", ({content, ok}) => {
    ok && content.push(componentAsPrioritisedFunction(Render))
})
