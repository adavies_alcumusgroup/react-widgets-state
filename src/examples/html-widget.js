import React, {useState} from "react"
import {componentAsPrioritisedFunction, widgets} from "../component/index"
import {createDefaultItem} from "./dashboard-items"
import injectSheet from "react-jss"
import {Editor} from "react-draft-wysiwyg"
import {EditorState} from "draft-js"
import {convertFromHTML, convertToHTML} from "draft-convert"
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'


widgets.on("dashboardItems.types", types => {
    types.push({
        name: "Text",
        create: () => createDefaultItem({type: "text"}),
    })
})

const style = {
    card: {
        marginBottom: 32,
        overflow: "hidden",
        minHeight: 20,
        borderRadius: 4,
    },
    editor: {
        height: "12em",
        border: "1px dotted gray",
        padding: 4,
        borderRadius: 4,
        marginBottom: 16,
    },
}

const Render = injectSheet(style)(({design: {item}, classes, editMode}) => {
    const {height = 3} = item

    let showOutline = editMode && (!item.html || !item.html.trim().length < 8)
    return (
        <div style={Object.assign({fontSize: height * 5}, showOutline ? {
            border: "1px dotted #444",
            height: "calc(100% - 32px)",
        } : null)} className={classes.card} dangerouslySetInnerHTML={{__html: item.html}}/>
    )
})


widgets.on("editor.dashboardItem.text", ({content, ok}) => {
    ok && content.push(componentAsPrioritisedFunction(Render))
})


const HtmlEditor = injectSheet(style)(function HtmlEditor({classes, focus: {selectedWidget}, queueUpdate}) {
    const [state, setState] = useState(EditorState.createWithContent(convertFromHTML(selectedWidget.html || "<p/>")))
    const [id, setId] = useState(selectedWidget.id)
    if (id !== selectedWidget.id) {
        setId(selectedWidget.id)
        setState(EditorState.createWithContent(convertFromHTML(selectedWidget.html || "<p/>")))
    }
    return (
        <div className="d-flex flex-column">
            <Editor editorClassName={classes.editor} editorState={state} onEditorStateChange={(state) => {
                try {
                    setState(state)
                    selectedWidget.html = convertToHTML(state.getCurrentContent())
                    queueUpdate()
                } catch (e) {
                    queueUpdate.cancel()
                }

            }}/>
        </div>
    )
})

widgets.on("edit.dashboard", ({focus: {selectedWidget}, editor: {tabs}}) => {
    if (selectedWidget && selectedWidget.type === "text") {
        tabs.text = tabs.text || {title: "Text", content: []}
        tabs.text.content.push(componentAsPrioritisedFunction(HtmlEditor))
    }
})
