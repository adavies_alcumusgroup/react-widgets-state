import {Col, Row} from "reactstrap"
import React from "react"
import {inPriorityOrder} from "./widgets"

export default function layoutRow({left = [], colType = "md", style, centreWidth, leftWidth = 3, rightWidth = 3, leftClass = "", rightClass = "", centreClass = "", centre = [], right = [], design, document, props, className, ...remainder}) {
    let leftInfo = {[colType]: leftWidth}
    let centreInfo = {[colType]: centreWidth || true}
    let rightInfo = {[colType]: rightWidth}
    return left.length === 0 && centre.length === 0 && right.length === 0 ? null : <Row className={className}>
        {left.length ? <Col {...leftInfo} style={{overflowY: "auto"}}>
            <div className={leftClass}>
                {left.sort(inPriorityOrder).map((Item, key) => <Item {...props} {...remainder}
                                                                     design={design} key={key}
                                                                     document={document}/>)}
            </div>

        </Col> : null}
        <Col {...centreInfo} style={Object.assign({overflowY: "auto"}, style)}>
            {centre.length ?

                <div className={centreClass}>
                    {centre && centre.sort(inPriorityOrder).map((Item, key) => <Item {...props} {...remainder}
                                                                                     design={design} key={key}
                                                                                     document={document}/>)}
                </div>
                : null}
        </Col>
        {right.length ? <Col {...rightInfo} style={{overflowY: "auto"}}>

            <div className={rightClass}>
                {right.sort(inPriorityOrder).map((Item, key) => <Item {...props} {...remainder}
                                                                      design={design} key={key}
                                                                      document={document}/>)}
            </div>

        </Col> : null}
    </Row>
}
