import isObject from "lodash/isObject"
import isFunction from "lodash/isFunction"

function createComparableString(o) {
    return Object.entries(o).map(([key, value]) => {
        if (isObject(value)) return createComparableString(value)
        if (isFunction(value)) value = value.toString()
        return `${key}:${"" + value}`
    }).join("|")
}

export default createComparableString
